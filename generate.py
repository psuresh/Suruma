#!/usr/bin/python
# Author: Suresh P <psuresh@gmx.com>
# Version: 0.3
# License: GPLv3

import getopt, sys, os
try:
    import fontforge
except ImportError:
    print "\tYou need fontforge python extension 'python-fontforge' package\n"

USAGE = """
	Usage: generate.py --format=otf|ttf [--with-serif] [--with-gasp] <file.sfd>
"""
try:
    opts, args = getopt.getopt(sys.argv[1:], "h", ["help", "format=", "with-serif", "with-gasp"])
except getopt.GetoptError, err:
    print str(err)
    print USAGE 
    sys.exit(2)
if not args :
    print USAGE
    sys.exit(2)
font_sfd_name = args[0]
if os.path.splitext(font_sfd_name)[1] != '.sfd':
     print USAGE
     print "\t" + font_sfd_name + " is not a .sfd file.\n"
     sys.exit(2)
build_ver = "beta10"
(font_name, font_ver_name) = os.path.splitext(font_sfd_name)[0].split("-", 1)
format = ''
with_gasp = False
style = 1

for o, a in opts:
    if o in ("-h", "--help"):
        print USAGE
        sys.exit()
    elif o == "--format":
        format = a
    elif o == "--with-serif":
        style = 2
        font_name += "Serif"
    elif o == "--with-gasp":
        with_gasp = True

if format not in ['otf', 'ttf']:
    print USAGE
    print "\tFormat should be 'otf' or 'ttf'\n"
    sys.exit(1)
else:
    font = fontforge.open(font_sfd_name)
    font.activeLayer = style
    font.fontname = font_name
    font.familyname = font_name
    font.fullname = font_name
    font.version += build_ver
    font_ver = font.version
    font_out_name = "-".join((font_name, font_ver_name))  + build_ver + "." + format
    font.appendSFNTName('English (US)','Preferred Family',  font_name)
    if format =="otf":
       font.layers[style].is_quadratic = False
       font.em = 1000
       font.selection.all()
       font.autoHint()
    else:
       if format == "ttf" :
           font.layers[style].is_quadratic = True
           font.em = 2048
           font.round()
           font.selection.all()
           font.autoHint()
           if with_gasp:
#              font.gasp = ((8, ('gridfit', 'antialias')),(16, ('gridfit', 'antialias')),)
               font.autoInstr()
    print "\nGenerating " + font_out_name + " ....."
    font.generate(font_out_name, flags=('opentype','apple','dummy-dsig'), layer=style)
    font.close()
if format == "ttf" and with_gasp:
    os.system("fontforge -script add-gasp.pe " + font_out_name)
sys.exit()
