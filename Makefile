FONT_NAME = Suruma
FONT_VERSION = 3.4

FONT_SRC_NAME = $(FONT_NAME)-$(FONT_VERSION).sfd
FONT_OTF_NAME = $(FONT_NAME)-$(FONT_VERSION).otf
FONT_TTF_NAME = $(FONT_NAME)-$(FONT_VERSION).ttf

FONT_GEN_SCRIPT = generate.pe
FONTFORGE_BIN = `which fontforge`
$TTF_OPT =

default: otf

all: otf ttf

otf: $(FONT_SRC_NAME)
	$(FONTFORGE_BIN) -script $(FONT_GEN_SCRIPT) --format ".otf" $(FONT_SRC_NAME) 
ttf: $(FONT_SRC_NAME)
	$(FONTFORGE_BIN) -script $(FONT_GEN_SCRIPT) --format ".ttf" $(FONT_SRC_NAME) $(TTF_OPT)

clean:
	rm -f *.otf
	rm -f *.ttf

